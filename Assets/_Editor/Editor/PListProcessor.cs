using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif


public static class PListProcessor
{

    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {

#if UNITY_IPHONE
        // Replace with your iOS AdMob app ID. Your AdMob App ID will look
        // similar to this sample ID: ca-app-pub-3940256099942544~1458002511
        string appId = "ca-app-pub-3234679428020521~9479712385";

        string plistPath = Path.Combine(path, "Info.plist");
        PlistDocument plist = new PlistDocument();

        plist.ReadFromFile(plistPath);
        plist.root.SetString("GADApplicationIdentifier", appId);

        var skAdNetworkArray = plist.root.CreateArray("SKAdNetworkItems");
        skAdNetworkArray.AddDict().SetString("SKAdNetworkIdentifier", "SU67R6K2V3.skadnetwork");

        var nsAppTransportSecurityDict = plist.root.CreateDict("NSAppTransportSecurity");
        nsAppTransportSecurityDict.SetBoolean("NSAllowsArbitraryLoads", true);

        File.WriteAllText(plistPath, plist.WriteToString());
#endif

    }

}