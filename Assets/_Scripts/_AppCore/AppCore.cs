namespace Floor18.HyperCasualTemplate.AppCore
{
    using System;
    using AppManagers;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Floor18.Gui;
    using Gui;
    using HCGameTools.Utilities;
    using RxECS.Core;
    using UniRx;
    using UnityEngine;

    /// <summary>
    /// Sets key app parameters and manages app launch logic 
    /// </summary>
    public class AppCore : MBWithLog
    {
        public static ReactiveProperty<bool> AppInitialized = new ReactiveProperty<bool>(false);

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private async void Start()
        {
            SetAppParameters();

            await Managers.AllManagersRegistration();
            await Systems.AllSystemsInitialization();

            if (AppParameters.LoadLevelInMenu)
            {
                MWorld mWorld = await Managers.ResolveAsync<MWorld>();
                await mWorld.LoadLevel();
            }

            AppInitialized.Value = true;
            
            var gui = await Modules.All.ResolveAsync<CGUI>();
            gui.DoFadeIn.Execute((0f, null));
            await TimeSpan.FromSeconds(0.5f);
            gui.DoFadeOut.Execute((0.5f, null));
            gui.GuiManager.Value.TryGetOrCreate<LobbyScreen>(DisplayMode.Screen);

#if !RELEASE_BUILD || USER_CHEAT
            CheatLogic.Instance.Init();
            gui.GuiManager.Value.TryGetOrCreate<CheatsScreen>(DisplayMode.Persistent);
#endif
        }

        private void SetAppParameters()
        {
            Application.targetFrameRate = 60;
            if (!Debug.isDebugBuild)
            {
                Input.multiTouchEnabled = false;
                Debug.unityLogger.logEnabled = false;
            }
        }
    }
}