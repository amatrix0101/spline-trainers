namespace Floor18.HyperCasualTemplate.AppCore
{
    using System;
    using System.Collections;
    using BPMobile.Common.Extensions;
    using Cysharp.Threading.Tasks;
    using DG.Tweening;
    using TMPro;
    using UniRx;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class AppLoader : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _loadingText;

        private CanvasGroup _cg;

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private async void OnEnable()
        {
            Observable.FromCoroutine(LoadingTextAnimation).Subscribe().AddTo(this);
            _cg = GetComponent<CanvasGroup>();
            await TimeSpan.FromTicks(1);
            await SceneManager.LoadSceneAsync(AppConsts.GameProd);
            await TimeSpan.FromTicks(1);
            await AppCore.AppInitialized.Where(v => v).First();
            _cg.DOFade(0f, 0.5f);
            await TimeSpan.FromSeconds(0.5f);
            Destroy(gameObject);
        }

        private IEnumerator LoadingTextAnimation()
        {
            int i = 0;
            while (true)
            {
                yield return new WaitForSeconds(0.3f);
                string dots = ".";
                if (i % 3 == 1)
                {
                    dots += ".";
                }
                else if (i % 3 == 2)
                {
                    dots += "..";
                }

                _loadingText.text = $"{AppConsts.Loading}{dots}";
                i++;
            }
        }
    }
}