namespace Floor18.HyperCasualTemplate.AppCore
{
    using HCGameTools.Utilities;
    using UnityEngine;

    public class AppParameters : MBWithLog
    {
        [SerializeField] private bool _loadLevelInMenu;

        public static bool LoadLevelInMenu;

        private void Awake()
        {
            LoadLevelInMenu = _loadLevelInMenu;
        }
        
    }
}