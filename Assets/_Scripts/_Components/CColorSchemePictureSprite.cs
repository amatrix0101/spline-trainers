namespace Floor18.HyperCasualTemplate.Components
{
    using System;
    using Configs;
    using RotaryHeart.Lib.SerializableDictionary;
    using RxECS.Core;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class SchemesSpritesDictionary : SerializableDictionaryBase<ColorSchemes, Sprite>
    {
    }

    public class CColorSchemePictureSprite : EntityComponent<CColorSchemePictureSprite>
    {
        [HideInInspector] public Image targetImage;
        [HideInInspector] public bool imageVariant;
        [HideInInspector] public SpriteRenderer targetSpriteRenderer;
        [HideInInspector] public bool spriteRendererVariant;

        public SchemesSpritesDictionary schemesSprites;

        protected override void OnComponentCreation()
        {
            targetImage = GetComponent<Image>();
            targetSpriteRenderer = GetComponent<SpriteRenderer>();
            imageVariant = targetImage != null;
            spriteRendererVariant = targetSpriteRenderer != null;
            if (!imageVariant && !spriteRendererVariant)
            {
                LogError(
                    $"[CColorSchemePictureColor]: {gameObject.name} doesn't contain either Image nor SpriteRenderer component!");
                Destroy(this);
            }
        }

        protected override void OnComponentEnable()
        {
        }

        protected override void OnComponentDisable()
        {
        }
        
    }
}