namespace Floor18.HyperCasualTemplate.Configs
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "CollectionPage", menuName = "HCTemplate/CollectionPage")]
    public class CollectionConfig: ScriptableObject
    {
        public List<SkinItemConfig> skinItemConfigs;
        public List<int> skinPrices;
        
        public int GetSkinItemPrice(int unlockedSkinItemsNumber)
        {
            return skinPrices[Mathf.Min(unlockedSkinItemsNumber, skinPrices.Count - 1)];
        }
    }
    
    [System.Serializable]
    public class SkinItemConfig
    {
        public Sprite preview;
        public int weight;
    }
}