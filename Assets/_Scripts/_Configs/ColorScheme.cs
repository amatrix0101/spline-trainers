namespace Floor18.HyperCasualTemplate.Configs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NaughtyAttributes;
    using UniRx;
    using UnityEngine;

    [Serializable]
    public class MaterialColor
    {
        [HideInInspector] public string materialName;
        [HideInInspector] public Material targetMaterial;
        public ColorReactiveProperty schemeColor;
    }

    [CreateAssetMenu(fileName = "ColorScheme", menuName = "HCTemplate/ColorScheme")]
    public class ColorScheme : ScriptableObject
    {
        [OnValueChanged("OnMaterialsListChanged")]
        public List<Material> ColoredMaterialsList = new List<Material>();

        [ReorderableList] public List<MaterialColor> SchemeColors = new List<MaterialColor>();

        private void OnValidate()
        {
            ColoredMaterialsList.RemoveAll(m => m == null);
            ColoredMaterialsList.ForEach(mat =>
            {
                if (mat == null) return;
                // ReSharper disable once SimplifyLinqExpressionUseAll (for readability)
                if (!SchemeColors.Any(c => c.targetMaterial == mat))
                {
                    var coloredMat = new MaterialColor();
                    coloredMat.targetMaterial = mat;
                    coloredMat.materialName = mat.name;
                    SchemeColors.Add(coloredMat);
                }
            });

            SchemeColors.RemoveAll(c => !ColoredMaterialsList.Contains(c.targetMaterial));
            List<MaterialColor> distinctList = new List<MaterialColor>();
            SchemeColors.ForEach(c =>
            {
                if (distinctList.Any(sc => sc.targetMaterial == c.targetMaterial)) return;
                distinctList.Add(c);
            });
            SchemeColors = distinctList;
        }
        
    }
}