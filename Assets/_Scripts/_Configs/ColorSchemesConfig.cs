namespace Floor18.HyperCasualTemplate.Configs
{
    using System;
    using NaughtyAttributes;
    using RotaryHeart.Lib.SerializableDictionary;
    using UnityEngine;

    public enum ColorSchemes
    {
        ColorScheme1,
        ColorScheme2,
        ColorScheme3,
    }

    public enum ColorSchemeChangeLogic
    {
        RandomEveryLevel,
        RandomEveryXCompletedLevels,
        ChangeWithLevelProgression,
    }

    [Serializable]
    public class MaterialsSchemesDictionary : SerializableDictionaryBase<ColorSchemes, ColorScheme>
    {
    }

    [CreateAssetMenu(fileName = "ColorSchemesConfig", menuName = "HCTemplate/ColorSchemesConfig")]
    public class ColorSchemesConfig : ScriptableObject
    {
        public bool forceSpecificScheme;
        [ShowIf("ForceSpecificScheme")] public ColorSchemes forcedScheme;

        [Space] public ColorSchemeChangeLogic changeLogic;

        [ShowIf("_showInterval")] public int interval = 1;

        public MaterialsSchemesDictionary materialsSchemes;


        // for "ShowIf" attribute
        private bool _showInterval => changeLogic == ColorSchemeChangeLogic.ChangeWithLevelProgression ||
                                      changeLogic == ColorSchemeChangeLogic.RandomEveryXCompletedLevels;
    }
    
}