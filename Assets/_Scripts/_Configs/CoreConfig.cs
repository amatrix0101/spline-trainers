namespace Floor18.HyperCasualTemplate.Configs
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "CoreConfig", menuName = "HCTemplate/CoreConfig")]
    public class CoreConfig : ScriptableObject
    {
        [Header("Meta")] 
        public int[] levelRewards;
        public int rateUsFrequency = 5;
        public int NotificationsPermissionLevel = 1;
    }
}