namespace Floor18.HyperCasualTemplate.Configs
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "DevConfig", menuName = "HCTemplate/DevConfig")]
    public class DevConfig : ScriptableObject
    {
        public bool enableDevPanel;
        public bool skipTutorial;
        public bool disableAds;
    }
}