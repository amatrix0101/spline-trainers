namespace Floor18.HyperCasualTemplate.Configs
{
    using NaughtyAttributes;
    using UnityEngine;

    [CreateAssetMenu(fileName = "DynamicDifficultyConfig", menuName = "HCTemplate/DynamicDifficultyConfig")]
    public class DynamicDifficultyConfig : ScriptableObject
    {
        [Header("Common parameters:")]
        [MinMaxSlider(-20, 20)]
        public Vector2 minMaxDifficulties;

        public int matchesCoundStartAfter = 0;
        public int winsInARowToIncreaseDifficulty = 1;
        public int losesInARowToDecreaseDifficulty = 1;

        private void OnValidate()
        {
            minMaxDifficulties.x = (int) minMaxDifficulties.x;
            minMaxDifficulties.y = (int) minMaxDifficulties.y;
            matchesCoundStartAfter = Mathf.Clamp(matchesCoundStartAfter, 0, int.MaxValue);
            winsInARowToIncreaseDifficulty = Mathf.Clamp(winsInARowToIncreaseDifficulty, 1, int.MaxValue);
            losesInARowToDecreaseDifficulty = Mathf.Clamp(losesInARowToDecreaseDifficulty, 1, int.MaxValue);
        }
        
    }
}