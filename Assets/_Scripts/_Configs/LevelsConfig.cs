namespace Floor18.HyperCasualTemplate.Configs
{
    using System.Collections.Generic;
    using BPMobile.Common;
    using DataContainers;
    using NaughtyAttributes;
    using UnityEngine;

    [CreateAssetMenu(fileName = "LevelsConfig", menuName = "HCTemplate/LevelsConfig")]
    public class LevelsConfig : ScriptableObject
    {
        public enum OnAllCompletedChoose
        {
            RandomFromAll,
            LoopAll,
            RandomFromInterval,
            LoopInterval,
        }

        [Space] 
        public OnAllCompletedChoose whenAllLevelsCompletedChoose;

        // for "ShowIf" attribute
        private bool _showInterval => whenAllLevelsCompletedChoose == OnAllCompletedChoose.LoopInterval ||
                                      whenAllLevelsCompletedChoose == OnAllCompletedChoose.RandomFromInterval;

        [ShowIf("_showInterval")] 
        public Int2 interval;

        [Space] 
        public Vector3 levelSpawnPosition;

        [HorizontalLine(color: EColor.Gray)] 
        public List<LevelData> levels;

        private void OnValidate()
        {
            interval.x = (int) interval.x;
            interval.y = (int) interval.y;
            interval.x = Mathf.Clamp(interval.x, 0, levels.Count - 1);
            interval.y = Mathf.Clamp(interval.y, 0, levels.Count - 1);
        }
    }
}