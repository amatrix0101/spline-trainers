namespace Floor18.HyperCasualTemplate.Configs
{
    using System;
    using RotaryHeart.Lib.SerializableDictionary;
    using UnityEngine;

    public enum MusicTypes
    {
        DefaultMusic,
    }

    public enum SoundTypes
    {
        UIButtonClick,
        LevelCompleted,
        LevelFailed,
        Collision,
    }

    [CreateAssetMenu(fileName = "SoundsConfig", menuName = "HCTemplate/SoundsConfig")]
    public class SoundsConfig : ScriptableObject
    {
        [Header("Music:")] public MusicDictionary music;

        [Header("Sounds:")] public SoundsDictionary sounds;
    }

    [Serializable]
    public struct AudioClipParameters
    {
        public AudioClip clip;
        [Range(0, 2)] public float volume;
    }

    [Serializable]
    public class MusicDictionary : SerializableDictionaryBase<MusicTypes, AudioClipParameters>
    {
    }

    [Serializable]
    public class SoundsDictionary : SerializableDictionaryBase<SoundTypes, AudioClipParameters>
    {
    }
}