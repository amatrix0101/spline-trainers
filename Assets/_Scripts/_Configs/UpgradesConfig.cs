namespace Floor18.HyperCasualTemplate.Configs
{
    using System;
    using UnityEngine;

    [CreateAssetMenu(fileName = "UpgradesConfig", menuName = "HCTemplate/UpgradesConfig")]
    public class UpgradesConfig : ScriptableObject
    {
        [Serializable]
        public struct UpgradeItem
        {
            public float baseValue;
            public float upgradeStep;
            public int[] upgradePrices;
        }

        public UpgradeItem[] upgrades;

        public float GetUpgradedItemValue(int id, int level)
        {
            return upgrades[id].baseValue + upgrades[id].upgradeStep * level;
        }
        
    }
}