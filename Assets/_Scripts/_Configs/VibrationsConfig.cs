namespace Floor18.HyperCasualTemplate.Configs
{
    using System;
    using MoreMountains.NiceVibrations;
    using RotaryHeart.Lib.SerializableDictionary;
    using UnityEngine;

    public enum VibrationsTypes
    {
        UIButtonClick,
        RoundWin,
        RoundLose,
    }

    [CreateAssetMenu(fileName = "VibrationsConfig", menuName = "HCTemplate/VibrationsConfig")]
    public class VibrationsConfig : ScriptableObject
    {
        [Header("Vibrations:")] public VibrationsDictionary vibrations;
    }

    [Serializable]
    public struct VibrationParameters
    {
        [Header("For Haptic supportable Androids and IOS:")]
        public HapticTypes hapticType;

        [Header("For other Androids:")] public bool useCustom;

        [Header("Custom android vibration settings:")]
        [Space(-10)]
        [Tooltip("In milliseconds")]
        public int duration;

        [Range(0, 255)] public int strength;
    }

    [Serializable]
    public class VibrationsDictionary : SerializableDictionaryBase<VibrationsTypes, VibrationParameters>
    {
    }
}