﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using System;
    using AppManagers;
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Floor18.Gui;
    using GameGraphics;
    using UnityEngine;
    using UnityEngine.UI;
    using UniRx;
    
    public class DevPanel : GuiController
    {
        [Space(5)]
        [SerializeField] private Button closeButton;
        [SerializeField] private Button previousLevelButton;
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button addMoneyButton;
        [SerializeField] private Button resetButton;
        [SerializeField] private Button qualityButton;
        [SerializeField] private Text qualityText;

        private MGame _game;
        private MData _data;
        private CQM _qm;

        protected override async void Init()
        {
            base.Init();

            _game = await Managers.ResolveAsync<MGame>();
            _data = await Managers.ResolveAsync<MData>();
            
            _qm = await Modules.All.ResolveAsync<CQM>();

            await Observable.TimerFrame(1);
            _canvas.additionalShaderChannels = AdditionalCanvasShaderChannels.TexCoord1
                                               | AdditionalCanvasShaderChannels.TexCoord2
                                               | AdditionalCanvasShaderChannels.TexCoord3;

            closeButton.OnClickAsObservable().Subscribe(_ => Close()).AddTo(ButtonBindings);

            previousLevelButton.OnClickAsObservable().Subscribe(_ =>
            {
                _data.Level.Value--;
                _game.OpenLobby();
            }).AddTo(ButtonBindings);

            nextLevelButton.OnClickAsObservable().Subscribe(_ =>
            {
                _data.Level.Value++;
                _game.OpenLobby();
            }).AddTo(ButtonBindings);

            addMoneyButton.OnClickAsObservable().Subscribe(_ =>
            {
                _data.Money.Value += 500;
            }).AddTo(ButtonBindings);

            resetButton.OnClickAsObservable().Subscribe(_ =>
            {
                _data.ResetData();
                Application.Quit();
            }).AddTo(ButtonBindings);

            qualityText.text = _qm.QualityManager.Value.Quality.ToString();
            qualityButton.OnClickAsObservable().Subscribe(_ =>
            {
                int q = (int) _qm.QualityManager.Value.Quality;
                q++;
                if (q >= Enum.GetValues(typeof(Quality)).Length)
                    q = 0;
                _qm.QualityManager.Value.SetQuality((Quality) q);
                qualityText.text = _qm.QualityManager.Value.Quality.ToString();
            }).AddTo(ButtonBindings);
        }
        
    }
}
