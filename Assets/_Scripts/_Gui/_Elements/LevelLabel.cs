namespace Floor18.HyperCasualTemplate.Gui
{
    using AppManagers;
    using BPMobile.Common.ManagersCore;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;

    public class LevelLabel : MonoBehaviour
    {
        [SerializeField] private Text label;

        private MData _data;
        
        private readonly CompositeDisposable _lifetimeDisposables = new CompositeDisposable();

        private async void OnEnable()
        {
            _lifetimeDisposables.Clear(); //Нужно чтобы избежать двойных подписок при первом запуске игры
            
            _data = await Managers.ResolveAsync<MData>();

            _data.Level.Subscribe(val =>
            {
                label.text = "LEVEL " + (val + 1);
            }).AddTo(_lifetimeDisposables);
        }

        private void OnDisable()
        {
             _lifetimeDisposables.Clear();
        }
    }
}