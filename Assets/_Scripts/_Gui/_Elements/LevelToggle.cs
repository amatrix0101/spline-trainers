namespace Floor18.HyperCasualTemplate.Gui
{
    using BPMobile.Common.Extensions;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class LevelToggle: MonoBehaviour
    {
        [SerializeField] private GameObject[] states;
        [SerializeField] private Text[] levelNum;
        
        public void Init(int lvlNum, int state)
        {
            for (int i = 0; i < states.Length; i++)
            {
                states[i].SetActive(state == i);
            }

            levelNum.ForEach(val => val.text = lvlNum.ToString());
        }
        
    }
}