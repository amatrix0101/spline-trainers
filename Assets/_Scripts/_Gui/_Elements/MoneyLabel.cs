namespace Floor18.HyperCasualTemplate.Gui
{
    using AppManagers;
    using BPMobile.Common.ManagersCore;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class MoneyLabel : MonoBehaviour
    {
        [SerializeField] private Text label;

        private MData _data;
        
        private readonly CompositeDisposable _lifetimeDisposables = new CompositeDisposable();

        private async void Start()
        {
            _lifetimeDisposables.Clear(); //Нужно чтобы избежать двойных подписок при первом запуске игры
            
            _data = await Managers.ResolveAsync<MData>();

            _data.Money.Subscribe(val =>
            {
                label.text = val.ToString();
            }).AddTo(_lifetimeDisposables);
        }
        
        private void OnDisable()
        {
            _lifetimeDisposables.Clear();
        }
    }
}