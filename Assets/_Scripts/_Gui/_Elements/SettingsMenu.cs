namespace Floor18.HyperCasualTemplate.Gui
{
    using AppManagers;
    using BPMobile.Common.ManagersCore;
    using Configs;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class SettingsMenu : MonoBehaviour
    {
        [SerializeField] private Button settingsButton;
        [SerializeField] private GameObject settingsMenu;
        [SerializeField] private Toggle soundToggle;
        [SerializeField] private Toggle vibroToggle;
        [SerializeField] private Button rateUsButton;
        
        private MData _data;
        private MRateUs _rateUs;
        
        private readonly CompositeDisposable _lifetimeDisposables = new CompositeDisposable();

        private async void OnEnable()
        {
            _lifetimeDisposables.Clear(); //Нужно чтобы избежать двойных подписок при первом запуске игры
            
            _data = await Managers.ResolveAsync<MData>();
            _rateUs = await Managers.ResolveAsync<MRateUs>();

            settingsButton.OnClickAsObservable()
                .Subscribe(_ => settingsMenu.SetActive(!settingsMenu.activeInHierarchy))
                .AddTo(_lifetimeDisposables);

            _data.SoundEnabled.Subscribe(val => soundToggle.isOn = val).AddTo(_lifetimeDisposables);
            _data.VibrationEnabled.Subscribe(val => vibroToggle.isOn = val).AddTo(_lifetimeDisposables);

            soundToggle.OnValueChangedAsObservable().Subscribe(val =>
            {
                _data.SoundEnabled.Value = val;
                Vibrations.PlayVibration(VibrationsTypes.UIButtonClick);
                Sound.PlaySound(SoundTypes.UIButtonClick);
            }).AddTo(_lifetimeDisposables);

            vibroToggle.OnValueChangedAsObservable().Subscribe(val =>
            {
                _data.VibrationEnabled.Value = val;
                Vibrations.PlayVibration(VibrationsTypes.UIButtonClick);
                Sound.PlaySound(SoundTypes.UIButtonClick);
            }).AddTo(_lifetimeDisposables);

            rateUsButton.OnClickAsObservable().Subscribe(_ =>
            {
                settingsMenu.SetActive(false);
                _rateUs.RateUs.Execute();
                Vibrations.PlayVibration(VibrationsTypes.UIButtonClick);
                Sound.PlaySound(SoundTypes.UIButtonClick);
            }).AddTo(_lifetimeDisposables);
        }
        
        private void OnDisable()
        {
            settingsMenu.SetActive(false);
            _lifetimeDisposables.Clear();
        }
        
    }
    
}