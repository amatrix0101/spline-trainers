namespace Floor18.HyperCasualTemplate.Gui
{
    using System;
    using AppManagers;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UICollectionItem : MonoBehaviour
    {
        [SerializeField] Button button;
        [SerializeField] private Image searchBorder;
        [SerializeField] private GameObject selectedView;
        [SerializeField] private GameObject lockedView;
        [SerializeField] private Image preview;
        [SerializeField] private GameObject unlockFXPrefab;

        public int Id { get; set; }

        public readonly BoolReactiveProperty IsUnlocked = new BoolReactiveProperty();

        public async void Init(int id, Sprite previewSprite, Action onClick)
        {
            Id = id;

            var data = await Managers.ResolveAsync<MData>();

            data.SelectedSkinId.Subscribe(x =>
            {
                selectedView.SetActive(x == Id);
            });
            
            IsUnlocked.Value = data.SkinItemsData[Id].IsUnlocked;
            
            IsUnlocked.Subscribe(x =>
            {
                lockedView.SetActive(!x);
                if (x)
                {
                    button.OnClickAsObservable().Subscribe(_ => onClick());
                }
            });

            preview.sprite = previewSprite;
        }

        public void Unlock()
        {
            Instantiate(unlockFXPrefab, transform);
            IsUnlocked.Value = true;
        }


        public async void SetRandomBorder(float duration)
        {
            searchBorder.color = Color.white;
            await TimeSpan.FromSeconds(duration);
            searchBorder.color = Color.clear;
        }
        
    }
}