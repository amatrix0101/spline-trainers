namespace Floor18.HyperCasualTemplate.Gui
{
    using AppManagers;
    using BPMobile.Common.ManagersCore;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UpgradeButton : MonoBehaviour
    {
        [SerializeField] private Button buyButton;
        [SerializeField] private GameObject maxButton;
        [SerializeField] private Text price;
        [SerializeField] private Text levelLabel;

        private int _id;
        private int[] _prices;
        private bool _max;
        
        private MData _data;
        private MConfig _config;

        private readonly CompositeDisposable _currentDisposable = new CompositeDisposable();
        
        private void OnDisable()
        {
            _currentDisposable.Clear();
        }

        public async void Init(int id)
        {
            _currentDisposable.Clear();
            _data = await Managers.ResolveAsync<MData>();
            _config = await Managers.ResolveAsync<MConfig>();

            _id = id;

            _prices = _config.Upgrades.upgrades[id].upgradePrices;

            buyButton.OnClickAsObservable().Subscribe(_ =>
            {
                _data.Money.Value -= _prices[_data.UpgradeLevels[id]];
                OnBuy();
            }).AddTo(_currentDisposable);

            UpdateView();
            
            _data.Money.Subscribe(OnMoneyChanged).AddTo(_currentDisposable);
        }
        
        private void OnMoneyChanged(int currentMoney)
        {
            if (_max) 
                return;
            
            bool canBuy = _prices[_data.UpgradeLevels[_id]] <= currentMoney;
            buyButton.interactable = canBuy;
        }

        private void OnBuy()
        {
            _data.UpgradeLevels[_id]++;
            _data.OnUpgrade.Execute();

            UpdateView();
        }

        private void UpdateView()
        {
            levelLabel.text = "LEVEL " + (_data.UpgradeLevels[_id] + 1);
            if (_data.UpgradeLevels[_id] >= _prices.Length)
            {
                _max = true;
                maxButton.SetActive(true);
                buyButton.gameObject.SetActive(false);
                return;
            }
            maxButton.SetActive(false);
            buyButton.gameObject.SetActive(true);
            price.text = _prices[_data.UpgradeLevels[_id]].ToString();
            OnMoneyChanged(_data.Money.Value);
        }
        
    }
}