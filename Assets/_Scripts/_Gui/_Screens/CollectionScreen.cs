﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using Floor18.Gui;
    using HyperCasualTemplate.Configs;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.UI.Extensions;

    public class CollectionScreen : GuiController
    {
        [Space(10)] 
        [SerializeField] private GameObject pagePrefab;
        [SerializeField] private UICollectionItem itemPrefab;
        [SerializeField] private GameObject pageTogglePrefab;
        [SerializeField] private HorizontalScrollSnap scroll;
        [SerializeField] private RectTransform pagesParent;
        [SerializeField] private RectTransform paginationParent;
        [SerializeField] private RectTransform viewport;
        [Space(5)] 
        [SerializeField] private Button closeButton;
        [SerializeField] private Button buyButton;
        [SerializeField] private Text priceText;
        [Space(5)] 
        [SerializeField] private AnimationCurve winAnimationSpeedCurve;

        private MCollection _collection;
        private MData _data;
        private MConfig _config;

        private List<List<UICollectionItem>> _items;
        private float _skinPrice;

        protected override async void Awake()
        {
            base.Awake();
            base.Init();

            _config = await Managers.ResolveAsync<MConfig>();
            _data = await Managers.ResolveAsync<MData>();
            _collection = await Managers.ResolveAsync<MCollection>();

            _items = new List<List<UICollectionItem>>();
            foreach (var page in _collection.Items)
            {
                Instantiate(pageTogglePrefab, paginationParent);
                var items = new List<UICollectionItem>();
                _items.Add(items);
                GameObject pageUI = Instantiate(pagePrefab, pagesParent);
                foreach (var item in page)
                {
                    UICollectionItem newItem = Instantiate(itemPrefab, pageUI.transform);
                    newItem.Init(item.Id, item.Preview, () => _data.SelectedSkinId.Value = item.Id);
                    items.Add(newItem);
                }
            }

            if (_collection.Items.Count <= 1)
            {
                scroll.GetComponent<ScrollRect>().horizontal = false;
                paginationParent.gameObject.SetActive(false);
            }

            scroll.UpdateLayout();
            scroll.OnSelectionPageChangedEvent.AddListener(_ => UpdateBuyButton());
        }

        public async void OpenPage(int id)
        {
            await TimeSpan.FromSeconds(0.1f);
            scroll.GoToScreen(id);
        }

        protected override void Init()
        {
            base.Init();

            _collection.OnItemBought.Subscribe(OnSkinBought).AddTo(ButtonBindings);
            _collection.OnItemUnlocked.Subscribe(OnSkinUnlocked).AddTo(ButtonBindings);

            closeButton.OnClickAsObservable().Subscribe(_ =>
            {
                Vibrations.PlayVibration(VibrationsTypes.UIButtonClick);
                Sound.PlaySound(SoundTypes.UIButtonClick);
                Close();
            }).AddTo(ButtonBindings);

            buyButton.OnClickAsObservable().Subscribe(_ =>
            {
                Vibrations.PlayVibration(VibrationsTypes.UIButtonClick);
                Sound.PlaySound(SoundTypes.UIButtonClick);
                _collection.BuyRandomSkinItem.Execute(scroll.CurrentPage);
            }).AddTo(ButtonBindings);


            _data.Money.Subscribe(value => UpdateBuyButton()).AddTo(ButtonBindings);

            foreach (var page in _items)
            {
                foreach (var item in page)
                {
                    item.IsUnlocked.Value = _collection.GetItem(item.Id).IsUnlocked;
                }
            }

            _data.SelectedSkinId.SetValueAndForceNotify(_collection.SelectedItem.Value.Id);

            var height = _items[0][0].transform.parent.GetComponent<RectTransform>().rect.height;
            viewport.sizeDelta = new Vector2(viewport.sizeDelta.x, height);
            UpdateBuyButton();
        }

        private void UpdateBuyButton()
        {
            var pageNum = scroll.CurrentPage;
            buyButton.gameObject.SetActive(_collection.Items[pageNum].Any(x => !x.IsUnlocked));
            priceText.text = GetPrice(pageNum).ToString();
            buyButton.interactable = GetPrice(pageNum) <= _data.Money.Value;
        }

        private void OnSkinUnlocked(CollectionItem item)
        {
            GetItem(item.Id).Unlock();
            _data.SelectedSkinId.Value = item.Id;
            UpdateBuyButton();
        }

        private async void OnSkinBought(CollectionItem item)
        {
            var pageNum = scroll.CurrentPage;
            if (_collection.Items[pageNum].Count(x => !x.IsUnlocked) > 0)
            {
                buyButton.interactable = false;
                closeButton.interactable = false;
                await Observable.FromCoroutine(SkinRandomizer).First();
                closeButton.interactable = true;
            }

            _collection.OnItemUnlocked.Execute(item);
        }

        private IEnumerator SkinRandomizer()
        {
            int n = 0;

            var lockedItems = _items[scroll.CurrentPage].Where(x => !x.IsUnlocked.Value).ToList();
            UICollectionItem randomItem = null;
            var randomCount = lockedItems.Count + 10;

            while (n < randomCount)
            {
                randomItem = lockedItems.Where(x => x != randomItem).ToList().RandomElement();

                float time = winAnimationSpeedCurve.Evaluate((float) n / randomCount) / 5f;
                randomItem.SetRandomBorder(time);
                n++;
                yield return new WaitForSeconds(time);
            }
        }

        private UICollectionItem GetItem(int id)
        {
            foreach (var page in _items)
            {
                foreach (var item in page)
                {
                    if (item.Id == id)
                        return item;
                }
            }

            return null;
        }

        private int GetPrice(int pageNum)
        {
            return _config.CollectionPages[pageNum]
                .GetSkinItemPrice(_collection.Items[pageNum].Count(x => x.IsUnlocked));
        }
    }
}