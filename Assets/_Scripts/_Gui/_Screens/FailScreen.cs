﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using BPMobile.Common.ManagersCore;
    using Floor18.Gui;
    using HyperCasualTemplate.Configs;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
    using UnityEngine;

    public class FailScreen : GuiController
    {
        [SerializeField] private SpecialButton continueBtn;

        private MGame _game;

        protected override async void Init()
        {
            base.Init();

            _game = await Managers.ResolveAsync<MGame>();

            continueBtn.OnClickAsObservable().First().Subscribe(_ =>
            {
                _game.OpenLobby(false);
            }).AddTo(ButtonBindings);
            
            Sound.PlaySound(SoundTypes.LevelFailed);
        }
        
    }
}