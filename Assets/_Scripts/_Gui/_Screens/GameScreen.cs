﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Misc;
    using BPMobile.Common.Modules;
    using BPMobile.Common.Modules.Input;
    using Floor18.Gui;
    using Floor18.Gui.Canvases;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
    using UnityEngine;
    
    public class GameScreen : GuiController
    {
        [SerializeField] private RectTransform tutorialFinger;
        [SerializeField] private GameObject tutorialFingerGO;

        private CInput _input;
        private MGame _game;
        private MConfig _config;

        private int _tapCount;
        private LTDescr _leanTweenDescr;

        protected override void Awake()
        {
            base.Awake();
            tutorialFingerGO = tutorialFinger.gameObject;
        }

        protected override async void Init()
        {
            base.Init();

            _game = await Managers.ResolveAsync<MGame>();
            _config = await Managers.ResolveAsync<MConfig>();
            
            _input = await Modules.All.ResolveAsync<CInput>();

            //CheckShowTutorial();
        }

        // private void CheckShowTutorial()
        // {
        //     if (!_game.CurrentLevel.Value.showTutorial) 
        //         return;
        //     
        //     _tapCount = 0;
        //     RectTransform canvasRect = StaticCanvas.Instance.CanvasRectTransform;
        //     Vector2 viewportPosition = Camera.main.WorldToViewportPoint(_game.CurrentLevel.Value.tutorFingerPos.position);
        //     Vector2 worldObjectScreenPosition = new Vector2(
        //         ((viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
        //         ((viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));
        //     tutorialFinger.anchoredPosition = worldObjectScreenPosition;
        //
        //     if (_leanTweenDescr != null) 
        //         LeanTween.cancel(_leanTweenDescr.id);
        //         
        //     tutorialFingerGO.transform.localScale = new Vector3(1f, 1f, 1f);
        //     tutorialFingerGO.SetActive(true);
        //     LeanTween.alpha(tutorialFinger, 1, 0.25f);
        //     _leanTweenDescr = LeanTween.scale(tutorialFinger, new Vector3(1.2f, 1.2f, 1.2f), 0.25f).setLoopType(LeanTweenType.pingPong);
        //     _input.OnTap.Subscribe(_ =>
        //     {
        //         _tapCount++;
        //         if (_tapCount >= _config.Core.tutorialTapCount)
        //         {
        //             LeanTween.cancel(_leanTweenDescr.id);
        //             LeanTween.alpha(tutorialFinger, 0, 0.25f).setOnComplete(() =>
        //             {
        //                 tutorialFingerGO.SetActive(false);
        //             });
        //         }
        //     }).AddTo(LifetimeDisposables);
        // }
        
    }
}
