﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Floor18.Gui;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;

    public class LobbyScreen : GuiController
    {
        [Space(10f)]
        [SerializeField] private Button collectionBtn;
        [SerializeField] private SpecialButton playBtn;
        [SerializeField] private LevelToggle[] levelToggles;

        private MGame _game;
        private MData _data;
        private CGUI _gui;

        protected override async void Init()
        {
            base.Init();

            _game = await Managers.ResolveAsync<MGame>();
            _data = await Managers.ResolveAsync<MData>();

            _gui = await Modules.All.ResolveAsync<CGUI>();

            playBtn.OnClickAsObservable().First().Subscribe(_ =>
            {
                _game.LaunchRound();
            }).AddTo(ButtonBindings);
            
            _data.Level.Subscribe(lvl =>
            {
                var order = lvl % levelToggles.Length;
                var startLevel = lvl - order;
                for (int i = 0; i < levelToggles.Length; i++)
                {
                    var state = 0;
                    if (i == order) 
                        state = 1;
                    if (i > order) 
                        state = 2;
                    levelToggles[i].Init(startLevel + i + 1, state);
                }
            }).AddTo(LifetimeDisposables);


            collectionBtn.OnClickAsObservable().Subscribe(_=>
            {
                _gui.GuiManager.Value.Show<CollectionScreen>(DisplayMode.DialogHideScreen);
            }).AddTo(ButtonBindings);


            /*
            devButton.gameObject.SetActive(false);

#if RELEASE_BUILD
#else
            devButton.gameObject.SetActive(true);

            devButton.OnClickAsObservable().Subscribe(_ =>
            {
                _guiManager.Show<DevPanel>();
            })
            .AddTo(ButtonBindings);
#endif
            */
        }
    }
}