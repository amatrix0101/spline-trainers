﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using Floor18.Gui;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class RateUsScreen : GuiController
    {
        [SerializeField] private Button backButton;
        [SerializeField] private Button rateUsButton;

        protected override void Init()
        {
            base.Init();

            backButton.OnClickAsObservable().Subscribe(_ => Close()).AddTo(ButtonBindings);

            rateUsButton.OnClickAsObservable().Subscribe(_ =>
            {
                Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
                Close();
            }).AddTo(ButtonBindings);
        }
        
    }
}