﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Floor18.Gui;
    using HyperCasualTemplate.Configs;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class WinScreen : GuiController
    {
        [SerializeField] private Text rewardText;
        [SerializeField] private SpecialButton continueBtn;

        private MData _data;
        private MConfig _config;
        private MGame _game;
        private CGUI _gui;
        
        protected override async void Init()
        {
            base.Init();

            _data = await Managers.ResolveAsync<MData>();
            _config = await Managers.ResolveAsync<MConfig>();
            _game = await Managers.ResolveAsync<MGame>();
            
            _gui = await Modules.All.ResolveAsync<CGUI>();

            continueBtn.OnClickAsObservable().First().Subscribe(__ =>
            {
                _game.OpenLobby(true);
            }).AddTo(ButtonBindings);

            rewardText.text =
                $"+{_config.Core.levelRewards[Mathf.Min(_config.Core.levelRewards.Length - 1, _data.Level.Value)]}";

            Sound.PlaySound(SoundTypes.LevelCompleted);
        }
        
    }
}