namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System.Collections;
    using BPMobile.Common.ManagersCore;
    using Cysharp.Threading.Tasks;
    using NaughtyAttributes;
    using UnityEngine;

    public class MCamera : Manager<MCamera>
    {
        [SerializeField, Required] private Camera _mainCamera;
        [SerializeField] private bool _applyReferenceOrthographicSize;

        public Camera MainCamera => _mainCamera;
        public Transform CamT => _camT ? _camT : _camT = _mainCamera.transform;

        private Transform _camT;

        protected override async UniTask ManagerInitialization()
        {
            if (_mainCamera == null)
            {
                LogError($"[CameraManager]: Main camera is not stated");
                AbortRegistration();
                return;
            }

            if (_applyReferenceOrthographicSize)
                ApplyReferenceOrthographicSize();
        }

        private void ApplyReferenceOrthographicSize()
        {
            var orthoCoef = _mainCamera.orthographicSize / 1920 * 1080;
            _mainCamera.orthographicSize = orthoCoef / Screen.width * Screen.height;
        }

        private IEnumerator LerpMoveTo(Vector3 targetPos, float duration)
        {
            Vector3 from = _camT.transform.position;
            float startTime = Time.time;
            float lerpCoef = 0;
            while (lerpCoef <= 1)
            {
                yield return null;
                lerpCoef = (Time.time - startTime) / duration;
                _camT.transform.position = Vector3.Lerp(from, targetPos, lerpCoef);
            }
        }

        private IEnumerator LerpLookTo(Vector3 targetLookDirection, float duration)
        {
            var from = Quaternion.LookRotation(_camT.transform.forward);
            var to = Quaternion.LookRotation(targetLookDirection);
            float startTime = Time.time;
            float lerpCoef = 0;
            while (lerpCoef <= 1)
            {
                yield return null;
                lerpCoef = (Time.time - startTime) / duration;
                _camT.transform.rotation = Quaternion.Lerp(from, to, lerpCoef);
            }
        }
        
    }
}