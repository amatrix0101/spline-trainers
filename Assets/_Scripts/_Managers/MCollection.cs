﻿namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System.Collections.Generic;
    using System.Linq;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Cysharp.Threading.Tasks;
    using Floor18.Gui;
    using Gui;
    using UnityEngine;
    using UniRx;

    public class CollectionItem : IWeighted
    {
        public int Id { get; set; }
        public Sprite Preview { get; set; }
        public int Weight { get; set; }
        public bool IsUnlocked { get; set; }
    }

    public class MCollection : Manager<MCollection>
    {
        private MConfig _config;
        private CGUI _guiManager;
        private MData _data;

        public readonly ReactiveCommand<int> BuyRandomSkinItem = new ReactiveCommand<int>();
        public readonly ReactiveCommand OpenIfAvailable = new ReactiveCommand();

        public readonly List<ReactiveCollection<CollectionItem>> Items = new List<ReactiveCollection<CollectionItem>>();

        public readonly ReactiveProperty<CollectionItem> SelectedItem = new ReactiveProperty<CollectionItem>();
        public readonly ReactiveCommand<CollectionItem> OnItemBought = new ReactiveCommand<CollectionItem>();
        public readonly ReactiveCommand<CollectionItem> OnItemUnlocked = new ReactiveCommand<CollectionItem>();

        protected override async UniTask ManagerInitialization()
        {
            _config = await Managers.ResolveAsync<MConfig>();
            _data = await Managers.ResolveAsync<MData>();
            _guiManager = await Modules.All.ResolveAsync<CGUI>();

            InitSkins();

            BuyRandomSkinItem.Subscribe(OnBuyRandomSkinItem).AddTo(LifetimeDisposables);
            OpenIfAvailable.Subscribe(_ => OpenIfEnoughMoney()).AddTo(LifetimeDisposables);
            OnItemUnlocked.Subscribe(item => _data.OnItemUnlocked.Execute(item)).AddTo(LifetimeDisposables);

            _data.SelectedSkinId.Subscribe(id => SelectedItem.Value = GetItem(id))
                .AddTo(LifetimeDisposables);
        }

        private void InitSkins()
        {
            Items.Clear();
            int id = 0;

            foreach (var page in _config.CollectionPages)
            {
                Items.Add(new ReactiveCollection<CollectionItem>());

                foreach (var skinItemConfig in page.skinItemConfigs)
                {
                    var skinItem = new CollectionItem
                    {
                        Id = id,
                        Preview = skinItemConfig.preview,
                        Weight = skinItemConfig.weight,
                        IsUnlocked = _data.SkinItemsData[id].IsUnlocked,
                    };
                    Items.Last().Add(skinItem);
                    id++;
                }
            }
        }

        private void OnBuyRandomSkinItem(int pageNum)
        {
            var lockedItems = Items[pageNum].Where(x => !x.IsUnlocked).ToList();
            var price = GetPrice(pageNum);

            if (lockedItems.Any() && _data.Money.Value >= price)
            {
                _data.Money.Value -= price;

                var randomSkin = lockedItems.GetRandomWeight() as CollectionItem;
                randomSkin.IsUnlocked = true;
                OnItemBought.Execute(randomSkin);
            }
        }

        private void OpenIfEnoughMoney()
        {
            if (GetLockedItems().Count == 0)
            {
                return;
            }

            for (var i = 0; i < _config.CollectionPages.Count; i++)
            {
                if (_data.Money.Value < GetPrice(i)) continue;
                var ui = _guiManager.GuiManager.Value.Show<CollectionScreen>(DisplayMode.Screen);
                ui.OpenPage(i);
                return;
            }
        }

        private int GetPrice(int pageNum)
        {
            return _config.CollectionPages[pageNum].GetSkinItemPrice(Items[pageNum].Count(x => x.IsUnlocked));
        }

        public CollectionItem GetItem(int id)
        {
            foreach (var page in Items)
            {
                foreach (var item in page)
                {
                    if (item.Id == id)
                        return item;
                }
            }

            return null;
        }

        public List<CollectionItem> GetLockedItems()
        {
            List<CollectionItem> result = new List<CollectionItem>();
            Items.ForEach(page => result.AddRange(page.Where(item => !item.IsUnlocked)));
            return result;
        }
        
    }
}