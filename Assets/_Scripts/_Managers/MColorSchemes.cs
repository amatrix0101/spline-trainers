namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System;
    using System.Collections.Generic;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using NaughtyAttributes;
    using UniRx;
    using UnityEngine;

    public class MColorSchemes : Manager<MColorSchemes>
    {
        private ReactiveProperty<ColorSchemes> _currentColorScheme = new ReactiveProperty<ColorSchemes>();

        public ReadOnlyReactiveProperty<ColorSchemes> CurrentColorScheme =>
            _currentColorScheme.ToReadOnlyReactiveProperty();

        [SerializeField, Required] private ColorSchemesConfig _schemesConfig;

        private Dictionary<Material, Color> _defaultMatColors = new Dictionary<Material, Color>();
        private CompositeDisposable _schemeDisposable = new CompositeDisposable();

        private MData _data;
        private MGame _game;
        private MWorld _world;

        protected override async UniTask ManagerInitialization()
        {
            if (_schemesConfig == null)
            {
                LogError($"[ColorSchemesManager]: color schemes config is not stated");
                AbortRegistration();
                return;
            }

            _data = await Managers.ResolveAsync<MData>();

            if (_schemesConfig.forceSpecificScheme)
            {
                _currentColorScheme.Value = _schemesConfig.forcedScheme;
                ApplyColorSchemeToMaterials();
                return;
            }

            if (_schemesConfig.changeLogic == ColorSchemeChangeLogic.ChangeWithLevelProgression)
            {
                int schemeIndex = _data.Level.Value / _schemesConfig.interval;
                schemeIndex %= EnumExtension.EntriesCount<ColorSchemes>();
                _currentColorScheme.Value = (ColorSchemes) schemeIndex;
            }
            else
            {
                _currentColorScheme.Value = EnumExtension.RandomFrom<ColorSchemes>();
            }

            ApplyColorSchemeToMaterials();

            _currentColorScheme
                .Skip(1)
                .Subscribe(_ => ApplyColorSchemeToMaterials())
                .AddTo(LifetimeDisposables);

            _world = await Managers.ResolveAsync<MWorld>();
            _game = await Managers.ResolveAsync<MGame>();

            switch (_schemesConfig.changeLogic)
            {
                case ColorSchemeChangeLogic.RandomEveryLevel:
                    BindRandomEveryLevel();
                    break;
                case ColorSchemeChangeLogic.RandomEveryXCompletedLevels:
                    BindRandomEveryXCompletedLevels();
                    break;
                case ColorSchemeChangeLogic.ChangeWithLevelProgression:
                    BindChangeWithLevelProgression();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void BindRandomEveryLevel()
        {
            _world.LevelReloading
                .Subscribe(_ =>
                    _currentColorScheme.Value = EnumExtension.RandomFromWithExclude(_currentColorScheme.Value))
                .AddTo(LifetimeDisposables);
        }

        private void BindRandomEveryXCompletedLevels()
        {
            bool shouldUpdateCS = false;
            int completedWithoutChange = 0;
            _game.OnRoundFinished
                .Where(v => v)
                .Do(_ => completedWithoutChange++)
                .Where(_ => completedWithoutChange >= _schemesConfig.interval)
                .Subscribe(_ => shouldUpdateCS = true)
                .AddTo(LifetimeDisposables);

            _world.LevelReloading
                .Subscribe(_ =>
                {
                    if (!shouldUpdateCS) return;
                    completedWithoutChange = 0;
                    shouldUpdateCS = false;
                    _currentColorScheme.Value = EnumExtension.RandomFromWithExclude(_currentColorScheme.Value);
                })
                .AddTo(LifetimeDisposables);
        }

        private void BindChangeWithLevelProgression()
        {
            _world.LevelReloading
                .Subscribe(_ =>
                {
                    int schemeIndex = _data.Level.Value / _schemesConfig.interval;
                    schemeIndex %= EnumExtension.EntriesCount<ColorSchemes>();
                    _currentColorScheme.Value = (ColorSchemes) schemeIndex;
                }).AddTo(LifetimeDisposables);
        }

        private void CacheMaterialsDefaultColors(ColorSchemes scheme)
        {
            _defaultMatColors.Clear();
            if (!_schemesConfig.materialsSchemes.ContainsKey(scheme)) 
                return;
            List<Material> affectedMaterials = _schemesConfig.materialsSchemes[scheme].ColoredMaterialsList;
            affectedMaterials.ForEach(mat => _defaultMatColors.Add(mat, mat.color));
        }

        private void RestoreMaterialsDefaultColors()
        {
            _schemeDisposable.Clear();
            _defaultMatColors.ForEach(m => m.Key.color = m.Value);
        }

        private void ApplyColorSchemeToMaterials()
        {
            _schemeDisposable.Clear();
            RestoreMaterialsDefaultColors();
            var scheme = _currentColorScheme.Value;
            if (!_schemesConfig.materialsSchemes.ContainsKey(scheme)) 
                return;
            CacheMaterialsDefaultColors(scheme);
            _schemesConfig.materialsSchemes[scheme].SchemeColors
                .ForEach(matData =>
                {
                    matData.targetMaterial.color = matData.schemeColor.Value;
#if UNITY_EDITOR
                    matData.schemeColor
                        .Subscribe(v => matData.targetMaterial.color = v)
                        .AddTo(_schemeDisposable);
#endif
                });
        }

        protected override void OnManagerDisable()
        {
            _schemeDisposable.Clear();
            RestoreMaterialsDefaultColors();
        }
    }
}