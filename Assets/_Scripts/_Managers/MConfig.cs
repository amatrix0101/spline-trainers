namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System.Collections.Generic;
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using NaughtyAttributes;
    using UnityEngine;

    public class MConfig : Manager<MConfig>
    {
        [SerializeField, Required] private DevConfig _devConfig;
        [SerializeField, Required] private CoreConfig _coreConfig;
        [SerializeField, Required] private LevelsConfig _levelsConfig;
        [SerializeField, Required] private UpgradesConfig _upgradesConfig;
        [SerializeField] private List<CollectionConfig> _collectionPages;

        public DevConfig Dev => _devConfig;
        public CoreConfig Core => _coreConfig;
        public LevelsConfig Levels => _levelsConfig;
        public UpgradesConfig Upgrades => _upgradesConfig;
        public List<CollectionConfig> CollectionPages => _collectionPages;

        protected override async UniTask ManagerInitialization()
        {
            bool correctInit = true;
            if (_devConfig == null)
            {
                LogError($"DevConfig is not stated");
                correctInit = false;
            }

            if (_coreConfig == null)
            {
                LogError($"CoreConfig is not stated");
                correctInit = false;
            }

            if (_levelsConfig == null)
            {
                LogError($"LevelsConfig is not stated");
                correctInit = false;
            }

            if (_upgradesConfig == null)
            {
                LogError($"UpgradesConfig is not stated");
                correctInit = false;
            }

            if (_collectionPages == null)
            {
                LogError($"CollectionPages is not stated");
                correctInit = false;
            }

            if (!correctInit)
            {
                LogError($"Couldn't initialize due to errors");
                AbortRegistration();
                return;
            }
        }
        
    }
}