using System.Threading.Tasks;

namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules.Data;
    using Cysharp.Threading.Tasks;
    using UnityEngine;
    using UniRx;

    /// <summary>
    /// Responsible for writing game data to PlayerPrefs
    /// <para></para>
    /// <para>Contains ReactiveProperties to all writable data</para>
    /// </summary>
    public class MData : DataManager<MData>
    {
        [Header("Connected to PlayerPrefs data:")]
        public BoolReactiveProperty SoundEnabled = new BoolReactiveProperty();
        public BoolReactiveProperty VibrationEnabled = new BoolReactiveProperty();
        public IntReactiveProperty Level = new IntReactiveProperty();
        public IntReactiveProperty Money = new IntReactiveProperty();
        public BoolReactiveProperty TutorialCompleted = new BoolReactiveProperty();
        public IntReactiveProperty GamesPlayed = new IntReactiveProperty();
        public IntReactiveProperty SelectedSkinId = new IntReactiveProperty();
        
        // TODO
        public ReactiveCollection<SkinItemData> SkinItemsData = new ReactiveCollection<SkinItemData>();
        public ReactiveCollection<int> UpgradeLevels = new ReactiveCollection<int>();
        public ReactiveCommand OnUpgrade = new ReactiveCommand();
        public ReactiveCommand<CollectionItem> OnItemUnlocked = new ReactiveCommand<CollectionItem>();
        // -----

    #region Dynamic difficulty

        public readonly IntReactiveProperty RoundsPlayed = new IntReactiveProperty();
        public readonly IntReactiveProperty WinsInARow = new IntReactiveProperty();
        public readonly IntReactiveProperty LosesInARow = new IntReactiveProperty();

    #endregion

        protected override async UniTask ManagerInitialization()
        {
            BindBoolRP(SoundEnabled, nameof(SoundEnabled), true);
            BindBoolRP(VibrationEnabled, nameof(VibrationEnabled), true);
            BindIntRP(Level, nameof(Level));
            BindIntRP(Money, nameof(Money));
            BindBoolRP(TutorialCompleted, nameof(TutorialCompleted));
            BindIntRP(GamesPlayed, nameof(GamesPlayed));

            BindIntRP(RoundsPlayed, nameof(RoundsPlayed));
            BindIntRP(WinsInARow, nameof(WinsInARow));
            BindIntRP(LosesInARow, nameof(LosesInARow));
            BindIntRP(SelectedSkinId, nameof(SelectedSkinId));

            UpgradeLevels = await GetSerializedFromPrefs(nameof(UpgradeLevels), GetDefaultUpgradesData());
            SkinItemsData = await GetSerializedFromPrefs(nameof(SkinItemsData), GetDefaultSkinItemsData());

            OnUpgrade
                .Subscribe(_ =>
                    SetSerializableToPrefs(nameof(UpgradeLevels), UpgradeLevels))
                .AddTo(LifetimeDisposables);
            OnItemUnlocked.Subscribe(val =>
            {
                SkinItemsData[val.Id].IsUnlocked = true;
                SetSerializableToPrefs(nameof(SkinItemsData), SkinItemsData);
            }).AddTo(LifetimeDisposables);
        }
        
        private async UniTask<ReactiveCollection<int>> GetDefaultUpgradesData()
        {
            ReactiveCollection<int> collection = new ReactiveCollection<int>();

            var config = await Managers.ResolveAsync<MConfig>();
            for (int i = 0; i < config.Upgrades.upgrades.Length; i++) 
                collection.Add(0);

            return collection;
        }
        
        private async UniTask<ReactiveCollection<SkinItemData>> GetDefaultSkinItemsData()
        {
            ReactiveCollection<SkinItemData> collection = new ReactiveCollection<SkinItemData>();
            
            var config = await Managers.ResolveAsync<MConfig>();
            config.CollectionPages.ForEach(
                page => page.skinItemConfigs.ForEach(
                    item => collection.Add(new SkinItemData())
                )
            );
            collection[0].IsUnlocked = true;
            return collection;
        }

    }
    
    [System.Serializable]
    public class SkinItemData
    {
        public bool IsUnlocked;
    }
}