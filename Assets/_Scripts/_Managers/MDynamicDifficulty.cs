namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using NaughtyAttributes;
    using UniRx;
    using UnityEngine;

    public class MDynamicDifficulty : Manager<MDynamicDifficulty>
    {
        [SerializeField, Required] private DynamicDifficultyConfig _difConfig;

        private MData _data;
        private MGame _game;

        protected override async UniTask ManagerInitialization()
        {
            if (_difConfig == null)
            {
                LogError($"[DynamicDifficultyManager]: Difficulty config is not stated");
                AbortRegistration();
                return;
            }

            _data = await Managers.ResolveAsync<MData>();
            _game = await Managers.ResolveAsync<MGame>();
            _game.OnRoundFinished.Subscribe(AdjustDifficulty).AddTo(LifetimeDisposables);
        }

        private void AdjustDifficulty(bool roundWon)
        {
            _data.RoundsPlayed.Value++;
            if (_data.RoundsPlayed.Value <= _difConfig.matchesCoundStartAfter)
                return;

            if (roundWon)
            {
                _data.WinsInARow.Value++;
                _data.LosesInARow.Value = 0;
            }
            else
            {
                _data.WinsInARow.Value = 0;
                _data.LosesInARow.Value++;
            }

            if (roundWon && _data.WinsInARow.Value >= _difConfig.winsInARowToIncreaseDifficulty)
            {
                IncreaseDifficulty();
                _data.WinsInARow.Value = 0;
                _data.LosesInARow.Value = 0;
                return;
            }

            if (!roundWon && _data.LosesInARow.Value >= _difConfig.losesInARowToDecreaseDifficulty)
            {
                DecreaseDifficulty();
                _data.WinsInARow.Value = 0;
                _data.LosesInARow.Value = 0;
                return;
            }
        }

        private void IncreaseDifficulty()
        {
            // throw new NotImplementedException();
            // todo
        }

        private void DecreaseDifficulty()
        {
            // throw new NotImplementedException();
            // todo
        }

        public void ApplyDifficulty()
        {
            // throw new NotImplementedException();
            // todo
        }
    }
}