namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System;
    using AppCore;
    using BPMobile.Common.Extensions;
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Configs;
    using Cysharp.Threading.Tasks;
    using Floor18.Gui;
    using Gui;
    using UniRx;
    using UnityEngine;

    /// <summary>
    /// Responsible for starting and finishing game round
    /// </summary>
    public class MGame : Manager<MGame>
    {
        public ReactiveCommand OnRoundStarted = new ReactiveCommand();
        public ReactiveCommand<bool> OnRoundFinished = new ReactiveCommand<bool>();

        public ReactiveCommand WinConditionReached = new ReactiveCommand();
        public ReactiveCommand LoseConditionReached = new ReactiveCommand();

        private readonly CompositeDisposable _roundDisposables = new CompositeDisposable();

        private MWorld _mWorld;
        private MData _data;
        private MRateUs _rateUs;
        private CGUI _gui;

        protected override async UniTask ManagerInitialization()
        {
            _mWorld = await Managers.ResolveAsync<MWorld>();
            _data = await Managers.ResolveAsync<MData>();
            _rateUs = await Managers.ResolveAsync<MRateUs>();
            _gui = await Modules.All.ResolveAsync<CGUI>();
        }

        public async UniTask LaunchRound()
        {
            _roundDisposables.Clear();
            if (!AppParameters.LoadLevelInMenu)
            {
                _gui.GuiManager.Value.CloseAllExcept(typeof(CheatsScreen));
                _gui.DoFadeIn.Execute((1f, null));
                await _mWorld.LoadLevel();
                _gui.DoFadeOut.Execute((1f, null));
            }

            _gui.GuiManager.Value.Show<GameScreen>(DisplayMode.Screen);
            await StartGameplay();
            BindWin();
            BindLose();
            OnRoundStarted.Execute();
            _mWorld.LevelLoaded.Value = false;
            
            DevToDevHelper.Track.LevelStart(_data.Level.Value);
            GameAnalyticsHelper.Track.LevelStart(_data.Level.Value);
        }

        // Call only from UIWin/UIFail
        public async UniTask OpenLobby(bool victory = false)
        {
            _roundDisposables.Clear();
            _gui.GuiManager.Value.CloseAllExcept(typeof(CheatsScreen));
            _gui.DoFadeIn.Execute((1f, null));
            if (victory)
            {
                _data.Level.Value++;
            }
            await TimeSpan.FromSeconds(1f);
            if (AppParameters.LoadLevelInMenu)
            {
                await _mWorld.LoadLevel();
            }
            _gui.DoFadeOut.Execute((1f, null));
            _gui.GuiManager.Value.Show<LobbyScreen>(DisplayMode.Screen, (_) =>
            {
                _rateUs.RateUsCheck.Execute();
            });
        }

        private async UniTask EndRound(bool victory)
        {
            _roundDisposables.Clear();
            Vibrations.PlayVibration(victory ? VibrationsTypes.RoundWin : VibrationsTypes.RoundLose);
            await FinishGameplay(victory);
            OnRoundFinished.Execute(victory);

            if (victory)
            {
                _gui.GuiManager.Value.Show<WinScreen>(DisplayMode.Screen);
                DevToDevHelper.Track.PlayerLevel(_data.Level.Value);
                GameAnalyticsHelper.Track.LevelCompleted(_data.Level.Value);
            }
            else
            {
                _gui.GuiManager.Value.Show<FailScreen>(DisplayMode.Screen);
                DevToDevHelper.Track.LevelFail(_data.Level.Value);
                GameAnalyticsHelper.Track.LevelFailed(_data.Level.Value);
            }
        }

        /// <summary>
        /// Can bind different win conditions here
        /// Call EndRound(true) when win condition is reached
        /// </summary>
        private void BindWin()
        {
            WinConditionReached
                .First()
                .Subscribe(_ => EndRound(true))
                .AddTo(_roundDisposables);


            // example:
#if UNITY_EDITOR
            Observable.EveryUpdate()
                .Where(_ => Input.GetKeyDown(KeyCode.Space))
                .Subscribe(_ => WinConditionReached.Execute())
                .AddTo(_roundDisposables);
#endif
        }

        /// <summary>
        /// Can bind different lose conditions here
        /// Call EndRound(false) when lose condition is reached
        /// </summary>
        private void BindLose()
        {
            LoseConditionReached
                .First()
                .Subscribe(_ => EndRound(false))
                .AddTo(_roundDisposables);


            // example:
#if UNITY_EDITOR
            Observable.EveryUpdate()
                .Where(_ => Input.GetKeyDown(KeyCode.Return))
                .Subscribe(_ => LoseConditionReached.Execute())
                .AddTo(_roundDisposables);
#endif
        }

        /// <summary>
        /// Various on game start events like camera hover, characters spawn with animations, etc
        /// </summary>
        private async UniTask StartGameplay()
        {
        }

        /// <summary>
        /// Various on game finish events like camera hover, characters deaths with animations, etc
        /// </summary>
        private async UniTask FinishGameplay(bool victory)
        {
            await TimeSpan.FromSeconds(0.5f);
        }
        
    }
}