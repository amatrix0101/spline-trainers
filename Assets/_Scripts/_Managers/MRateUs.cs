namespace Floor18.HyperCasualTemplate.AppManagers 
{
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using Cysharp.Threading.Tasks;
    using Floor18.Gui;
    using Gui;
    using UniRx;
    using UnityEngine;
#if UNITY_IOS
    using UnityEngine.iOS;
#endif
    
    public class MRateUs : Manager<MRateUs>
    {
        private MData _data;
        private MConfig _config;
        private CGUI _guiManager;

        private readonly CompositeDisposable _lifetimeDisposables = new CompositeDisposable();
        
        public readonly ReactiveCommand RateUs = new ReactiveCommand();
        public readonly ReactiveCommand RateUsCheck = new ReactiveCommand();

        protected override async UniTask ManagerInitialization()
        {
            _data = await Managers.ResolveAsync<MData>();
            _config = await Managers.ResolveAsync<MConfig>();
            
            _guiManager = await Modules.All.ResolveAsync<CGUI>();

            RateUs.Subscribe(_ => OnRateUs()).AddTo(_lifetimeDisposables);
            RateUsCheck.Subscribe(_ => CheckForRateUs()).AddTo(_lifetimeDisposables);
        }
        
        private void CheckForRateUs()
        {
            if (_data.Level.Value > 0 && _data.Level.Value % _config.Core.rateUsFrequency == 0) 
                OnRateUs();
        }

        
        private void OnRateUs()
        {
#if UNITY_IOS && !UNITY_EDITOR
            Device.RequestStoreReview();
#else
            _guiManager.GuiManager.Value.Show<RateUsScreen>(DisplayMode.DialogHideScreen);
#endif
        }
        
    }
}