namespace Floor18.HyperCasualTemplate.AppManagers
{
    using System;
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using DataContainers;
    using UniRx;
    using UnityEngine;
    using Random = UnityEngine.Random;

    /// <summary>
    /// Responsible for spawning game objects
    /// <para></para>
    /// <para>Also contains references to key objects</para>
    /// </summary>
    public class MWorld : Manager<MWorld>
    {
        [HideInInspector] public ReactiveProperty<LevelData> CurrentLevel = new ReactiveProperty<LevelData>();
        [HideInInspector] public BoolReactiveProperty LevelLoaded = new BoolReactiveProperty();
        
        public ReactiveCommand LevelReloading = new ReactiveCommand();

        private MData _data;
        private MConfig _config;
        private LevelsConfig _levelsConfig;
        private LevelData _currentLevelObject;

        protected override async UniTask ManagerInitialization()
        {
            LevelLoaded.Value = false;
            _data = await Managers.ResolveAsync<MData>();
            _config = await Managers.ResolveAsync<MConfig>();
            _levelsConfig = _config.Levels;
        }

        public async UniTask LoadLevel()
        {
            await ClearLevel();
            LevelReloading.Execute();
            var level = GetLevel(_data.Level.Value);
            _currentLevelObject = Instantiate(level, _levelsConfig.levelSpawnPosition, Quaternion.identity);
            CurrentLevel.Value = _currentLevelObject;
            GC.Collect();
            await FillLevel();
            LevelLoaded.Value = true;
        }

        /// <summary>
        /// Fill level with the rest of objects if needed (for example from object pool)
        /// </summary>
        private async UniTask FillLevel()
        {
        }

        private async UniTask ClearLevel()
        {
            LevelLoaded.Value = false;
            if (_currentLevelObject != null) 
                Destroy(_currentLevelObject.gameObject);
        }

        private LevelData GetLevel(int levelIndex)
        {
            if (levelIndex < 0)
            {
                LogError("[MWorld] Level index is less than 0!");
                throw new IndexOutOfRangeException();
            }

            LevelData level;
            if (levelIndex < _levelsConfig.levels.Count)
            {
                level = _levelsConfig.levels[levelIndex];
            }
            else
            {
                int index = 0;
                switch (_levelsConfig.whenAllLevelsCompletedChoose)
                {
                    case LevelsConfig.OnAllCompletedChoose.RandomFromAll:
                        index = Random.Range(0, _levelsConfig.levels.Count);
                        break;
                    case LevelsConfig.OnAllCompletedChoose.LoopAll:
                        index = levelIndex % _levelsConfig.levels.Count;
                        break;
                    case LevelsConfig.OnAllCompletedChoose.RandomFromInterval:
                        index = Random.Range(_levelsConfig.interval.x, _levelsConfig.interval.y + 1);
                        break;
                    case LevelsConfig.OnAllCompletedChoose.LoopInterval:
                        int inIntervalIndex = (levelIndex - _levelsConfig.levels.Count) %
                                              (_levelsConfig.interval.y - _levelsConfig.interval.x + 1);
                        index = _levelsConfig.interval.x + inIntervalIndex;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                level = _levelsConfig.levels[index];
            }

            return level;
        }
        
    }
}