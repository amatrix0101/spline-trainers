namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using NaughtyAttributes;
    using UniRx;
    using UnityEngine;

    public class MSound : Manager<MSound>
    {
        [ReadOnly] public BoolReactiveProperty SoundEnabled = new BoolReactiveProperty();
        [ReadOnly] public BoolReactiveProperty MusicEnabled = new BoolReactiveProperty();

        public void PlaySound(SoundTypes soundType) => PlaySoundImplementation(soundType);
        public void PlaySound(AudioClip clip, float volume = 1f) => PlaySoundImplementation(clip, volume);
        public void PlayMusic(MusicTypes musicType) => PlayMusicImplementation(musicType);
        public void PauseMusic() => _musicSource.Pause();
        public void ResumeMusic() => _musicSource.UnPause();

        [SerializeField, Required] private SoundsConfig _soundsConfig;
        [SerializeField, Required] private AudioSource _musicSource;
        [SerializeField, Required] private AudioSource _soundSource;

        protected override async UniTask ManagerInitialization()
        {
            bool correctInit = true;
            if (_soundsConfig == null)
            {
                LogError($"[SoundManager]: SoundConfig is not stated");
                correctInit = false;
            }

            if (_musicSource == null)
            {
                LogError($"[SoundManager]: MusicSource is not stated");
                correctInit = false;
            }

            if (_soundSource == null)
            {
                LogError($"[SoundManager]: SoundSource is not stated");
                correctInit = false;
            }

            if (!correctInit)
            {
                LogError($"[SoundManager]: Couldn't initialize due to errors");
                AbortRegistration();
                return;
            }

            SoundEnabled
                .Subscribe(val =>
                {
                    _soundSource.mute = !val;
                    _musicSource.mute = !val;
                }).AddTo(LifetimeDisposables);

            var data = await Managers.ResolveAsync<MData>();
            data.SoundEnabled
                .Subscribe(isEnabled =>
                {
                    SoundEnabled.Value = isEnabled;
                    MusicEnabled.Value = isEnabled;
                }).AddTo(LifetimeDisposables);

            // var uiManager = await Managers.ResolveAsync<UIManager>();
            // uiManager.UiButtonClicked
            //     .Subscribe(_ => PlaySound(SoundTypes.UIButtonClick))
            //     .AddTo(LifetimeDisposables);
        }

        private void PlaySoundImplementation(SoundTypes soundType)
        {
            if (!_soundsConfig.sounds.ContainsKey(soundType))
            {
                LogError($"[SoundManager]: {soundType} sound is missing");
                return;
            }

            var soundClip = _soundsConfig.sounds[soundType];
            PlaySound(soundClip);
        }

        private void PlaySound(AudioClipParameters soundClip)
        {
            if (soundClip.clip == null)
            {
                LogError($"[SoundManager]: sound audio clip is missing");
                return;
            }

            if (!SoundEnabled.Value) 
                return;
            
            _soundSource.PlayOneShot(soundClip.clip, soundClip.volume);
        }

        private void PlaySoundImplementation(AudioClip clip, float volume = 1f)
        {
            if (!SoundEnabled.Value)
                return;
            
            _soundSource.PlayOneShot(clip, volume);
        }

        private void PlayMusicImplementation(MusicTypes musicType)
        {
            if (!_soundsConfig.music.ContainsKey(musicType))
            {
                LogError($"[SoundManager]: {musicType} music is missing");
                return;
            }

            var musicClip = _soundsConfig.music[musicType];
            PlayMusic(musicClip);
        }

        private void PlayMusic(AudioClipParameters musicClip)
        {
            if (musicClip.clip == null)
            {
                LogError($"[SoundManager]: music audio clip is missing");
                return;
            }

            if (musicClip.clip != _musicSource.clip)
            {
                _musicSource.clip = musicClip.clip;
                _musicSource.volume = musicClip.volume;
                _musicSource.loop = true;
            }

            if (!MusicEnabled.Value) 
                return;
            
            _musicSource.Play();
        }
        
    }
}