namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using Configs;
    using UnityEngine;

    public static class Sound
    {
        public static void PlaySound(SoundTypes soundType)
        {
            MSound mSound = Managers.Resolve<MSound>();
            if (mSound == null)
            {
                Debug.LogError($"[MSound]: Action failed. Sound Manager is not initialized yet.");
                return;
            }

            mSound.PlaySound(soundType);
        }

        public static void PlayMusic(MusicTypes musicType)
        {
            MSound mSound = Managers.Resolve<MSound>();
            if (mSound == null)
            {
                Debug.LogError($"[MSound]: Action failed. Sound Manager is not initialized yet.");
                return;
            }

            mSound.PlayMusic(musicType);
        }

        public static void PauseMusic()
        {
            MSound mSound = Managers.Resolve<MSound>();
            if (mSound == null)
            {
                Debug.LogError($"[MSound]: Action failed. Sound Manager is not initialized yet.");
                return;
            }

            mSound.PauseMusic();
        }

        public static void ResumeMusic()
        {
            MSound mSound = Managers.Resolve<MSound>();
            if (mSound == null)
            {
                Debug.LogError($"[MSound]: Action failed. Sound Manager is not initialized yet.");
                return;
            }

            mSound.ResumeMusic();
        }
    }
}