namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using Configs;
    using Cysharp.Threading.Tasks;
    using MoreMountains.NiceVibrations;
    using NaughtyAttributes;
    using UniRx;
    using UnityEngine;

    public class MVibrations : Manager<MVibrations>
    {
        [ReadOnly] public BoolReactiveProperty VibrationsEnabled = new BoolReactiveProperty();

        public void PlayVibration(VibrationsTypes vibrationType) => PlayVibrationImplementation(vibrationType);

        [SerializeField, Required] private VibrationsConfig _vibrationsConfig;

        [SerializeField] private bool _enableLog = false;

#if UNITY_ANDROID
        private float _lastVibrationStartTime;
#endif

        protected override async UniTask ManagerInitialization()
        {
            bool correctInit = true;
            if (_vibrationsConfig == null)
            {
                LogError($"[Vibrations] VibrationsConfig is not stated");
                correctInit = false;
            }

            if (!correctInit)
            {
                LogError($"[Vibrations] Couldn't initialize due to errors");
                AbortRegistration();
                return;
            }

#if UNITY_IOS
            MMVibrationManager.iOSInitializeHaptics();
#endif

            var data = await Managers.ResolveAsync<MData>();
            data.VibrationEnabled
                .Subscribe(isEnabled => VibrationsEnabled.Value = isEnabled)
                .AddTo(LifetimeDisposables);

            // var uiManager = await Managers.ResolveAsync<UIManager>();
            // uiManager.UiButtonClicked
            //     .Subscribe(_ => PlayVibration(VibrationsTypes.UIButtonClick))
            //     .AddTo(LifetimeDisposables);
        }

        protected override void OnManagerDisable()
        {
#if UNITY_IOS
            MMVibrationManager.iOSReleaseHaptics();
#endif
        }

        private void PlayVibrationImplementation(VibrationsTypes vibrationType)
        {
            if (!VibrationsEnabled.Value) return;

            if (!_vibrationsConfig.vibrations.ContainsKey(vibrationType))
            {
                LogError($"[Vibrations] {vibrationType} vibration is missing");
                return;
            }

            VibrationParameters vibrationParameters = _vibrationsConfig.vibrations[vibrationType];

#if UNITY_IOS
            if (!MMVibrationManager.HapticsSupported()) 
                return;
            MMVibrationManager.iOSTriggerHaptics(vibrationParameters.hapticType);
#endif
#if UNITY_ANDROID
            if (MMVibrationManager.HapticsSupported()) {
                MMVibrationManager.Haptic(vibrationParameters.hapticType);
                if (_enableLog)
                    Log($"[Vibrations] Played {vibrationType} vibration");
                return;
            }
            if (vibrationParameters.useCustom) {
                MMVibrationManager.AndroidVibrate(vibrationParameters.duration, vibrationParameters.strength);
            } else {
                if (Time.time - _lastVibrationStartTime > 0.011) {
                    MMVibrationManager.AndroidVibrate(10, 10);
                }
            }
            _lastVibrationStartTime = Time.time;
#endif
            if (_enableLog)
                Log($"[Vibrations] Played {vibrationType} vibration");
        }
        
    }
}