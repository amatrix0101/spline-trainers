namespace Floor18.HyperCasualTemplate.AppManagers
{
    using BPMobile.Common.ManagersCore;
    using Configs;
    using UnityEngine;

    public static class Vibrations
    {
        public static void PlayVibration(VibrationsTypes vibrationType)
        {
            MVibrations mVibration = Managers.Resolve<MVibrations>();
            if (mVibration == null)
            {
                Debug.LogError($"[MVibration]: Action failed. Vibration Manager is not initialized yet.");
                return;
            }

            mVibration.PlayVibration(vibrationType);
        }
        
    }
}