namespace Floor18.HyperCasualTemplate.Systems
{
    using BPMobile.Common.ManagersCore;
    using Components;
    using Cysharp.Threading.Tasks;
    using AppManagers;
    using RxECS.Core;
    using UniRx;
    using UnityEngine;

    public class CSColorSchemePictureColor : OnComponentSystem<CColorSchemePictureColor>
    {
        private MColorSchemes _mColorSchemes;

        protected override async UniTask OnSystemEnable()
        {
            _mColorSchemes = await Managers.ResolveAsync<MColorSchemes>();
        }

        protected override void OnSystemDisable()
        {
        }

        protected override void OnComponentRegistered(CColorSchemePictureColor component)
        {
            _mColorSchemes.CurrentColorScheme
                .Subscribe(scheme =>
                {
                    if (component.schemesColors.TryGetValue(scheme, out Color schemeColor))
                    {
                        if (component.imageVariant)
                            component.targetImage.color = schemeColor;
                        if (component.spriteRendererVariant)
                            component.targetSpriteRenderer.color = schemeColor;
                    }
                    else
                    {
                        LogError($"[CSColorSchemePictureColor]:" +
                                 $" {component.gameObject.name} doesn't have color entry for '{scheme}' color scheme.");
                    }
                }).AddTo(component.LifetimeDisposables);
        }

        protected override void OnComponentUnregistered(CColorSchemePictureColor component)
        {
        }

        protected override void OnSystemUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }

        protected override void OnSystemFixedUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }

        protected override void OnSystemLateUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }
        
    }
}