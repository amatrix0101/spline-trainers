namespace Floor18.HyperCasualTemplate.Systems
{
    using BPMobile.Common.ManagersCore;
    using Components;
    using Cysharp.Threading.Tasks;
    using AppManagers;
    using RxECS.Core;
    using UniRx;
    using UnityEngine;

    public class CSColorSchemePictureSprite : OnComponentSystem<CColorSchemePictureSprite>
    {
        private MColorSchemes _mColorSchemes;

        protected override async UniTask OnSystemEnable()
        {
            _mColorSchemes = await Managers.ResolveAsync<MColorSchemes>();
        }

        protected override void OnSystemDisable()
        {
        }

        protected override void OnComponentRegistered(CColorSchemePictureSprite component)
        {
            _mColorSchemes.CurrentColorScheme
                .Subscribe(scheme =>
                {
                    if (component.schemesSprites.TryGetValue(scheme, out Sprite schemeSprite))
                    {
                        if (component.imageVariant)
                            component.targetImage.sprite = schemeSprite;
                        if (component.spriteRendererVariant)
                            component.targetSpriteRenderer.sprite = schemeSprite;
                    }
                    else
                    {
                        LogError($"[CSColorSchemePictureSprite]:" +
                                 $" {component.gameObject.name} doesn't have sprite entry for '{scheme}' color scheme.");
                    }
                }).AddTo(component.LifetimeDisposables);
        }

        protected override void OnComponentUnregistered(CColorSchemePictureSprite component)
        {
        }

        protected override void OnSystemUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }

        protected override void OnSystemFixedUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }

        protected override void OnSystemLateUpdate()
        {
            // AffectedComponents.ForEach(component => { });
        }
        
    }
}