﻿using UnityEngine;

public static class DevToDevHelper
{
    public class EventNames
    {
        public const string Level_start = "Level_start";
        public const string Level_fail = "Level_fail";
    }


    public static class Track
    {
        public static bool LogToConsole;

        public static void PlayerLevel(int level)
        {
#if DEV_TO_DEV_ENABLED
            DevToDev.Analytics.LevelUp(level + 1);
            if (LogToConsole) Debug.Log($"[D2D]: Player level {level + 1}");
#endif
        }

        public static void LevelStart(int level)
        {
#if DEV_TO_DEV_ENABLED
            DevToDev.CustomEventParams eventParams = new DevToDev.CustomEventParams();
            eventParams.AddParam("level", level + 1);
            DevToDev.Analytics.CustomEvent(EventNames.Level_start, eventParams);
            if (LogToConsole) Debug.Log($"[D2D]: Level Start {level + 1}");
#endif
        }

        public static void LevelFail(int level)
        {
#if DEV_TO_DEV_ENABLED
            DevToDev.CustomEventParams eventParams = new DevToDev.CustomEventParams();
            eventParams.AddParam("level", level + 1);
            DevToDev.Analytics.CustomEvent(EventNames.Level_fail, eventParams);
            if (LogToConsole) Debug.Log($"[D2D]: Level Fail {level + 1}");
#endif
        }
    }
}