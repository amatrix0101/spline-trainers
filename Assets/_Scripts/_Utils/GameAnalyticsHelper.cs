using UnityEngine;

public static class GameAnalyticsHelper
{
    public static class Track
    {
        public static bool LogToConsole = true;

        public static void LevelStart(int level)
        {
#if GAME_ANALYTICS_ENABLED
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, (level+1).ToString());
                if (LogToConsole) Debug.Log($"[GA]: {GAProgressionStatus.Start} -> {level + 1}");
#endif
        }

        public static void LevelCompleted(int level)
        {
#if GAME_ANALYTICS_ENABLED
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, (level+1).ToString());
                if (LogToConsole) Debug.Log($"[GA]: {GAProgressionStatus.Complete} -> {level + 1}");
#endif
        }


        public static void LevelFailed(int level)
        {
#if GAME_ANALYTICS_ENABLED
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, (level+1).ToString());
                if (LogToConsole) Debug.Log($"[GA]: {GAProgressionStatus.Fail} -> {level + 1}");
#endif
        }
    }
}