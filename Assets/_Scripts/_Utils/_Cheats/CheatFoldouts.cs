using System.Collections.Generic;

public class CheatFoldouts : ScriptableSingleton<CheatFoldouts>
{
    public List<string> unfolded;

    protected override void OnCreate()
    {
        unfolded = new List<string>();
        unfolded.Add("root");
    }
}