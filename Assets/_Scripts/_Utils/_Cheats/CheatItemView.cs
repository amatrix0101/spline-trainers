﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utils;
    
    public class CheatItemView : MonoBehaviour
    {

#if !RELEASE_BUILD || USER_CHEAT
        [SerializeField]
        private Text _title;

        [SerializeField]
        private Button _button;

        [SerializeField]
        private Image _image;

        private CheatMenu _cheatMenu;
        private CheatItem _cheatItem;

        public void Init(CheatMenu cheatMenu, CheatItem cheatItem)
        {
            _cheatMenu = cheatMenu;
            _cheatItem = cheatItem;
            _title.text = _cheatItem.Title;
            _button.onClick.AddListener(OnClick);

            if (_cheatItem.Items.Count > 0)
                _title.text = "[ " + _title.text + " ]";
        }

        private void OnClick()
        {
            if (_cheatItem.Action != null)
            {
                try
                {
                   Floor18Log.Log("Execute cheat: " + _cheatItem.Title);
                    _cheatItem.Action();
                    if (!_cheatMenu.LockMode)
                        _cheatMenu.Close();
                }
                catch(Exception e)
                {
                    Floor18Log.LogException(e);
                    if (!_cheatMenu.LockMode)
                        _cheatMenu.Close();
                }
            }

            if (_cheatItem.ActionItemModify != null)
            {
                try
                {
                    Floor18Log.Log("Execute cheat: " + _cheatItem.Title);
                    _cheatItem.ActionItemModify(_cheatItem);
                    if (!_cheatMenu.LockMode)
                        _cheatMenu.Close();
                }
                catch (Exception e)
                {
                    Floor18Log.LogException(e);
                    if (!_cheatMenu.LockMode)
                        _cheatMenu.Close();
                }
            }

            if (_cheatItem.ActionInput != null)
                _cheatMenu.EnableInputMode(_cheatItem);

            if (_cheatItem.Items.Count > 0)
                _cheatMenu.OnForward(_cheatItem);     
        }

        void OnItemModify()
        {
            _title.text = _cheatItem.Title;
        }

        public void InitAsBack(CheatMenu cheatMenu, CheatItem cheatItemRoot, CheatItem cheatItem)
        {
            _cheatMenu = cheatMenu;
            _title.text = "<< " + cheatItemRoot.Title + " <<"; //\n__" + cheatItem.Title + "__";
            _button.onClick.AddListener(_cheatMenu.OnBack);
        }
#endif
    }
}
