namespace Floor18
{
    using System;
    using System.Collections.Generic;
    using BPMobile.Common.ManagersCore;
    using BPMobile.Common.Modules;
    using GameGraphics;
    using HyperCasualTemplate.AppManagers;
    using UniRx;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    public class CheatItem
    {

#if !RELEASE_BUILD || USER_CHEAT
        public string Title;
        public Action Action;
        public Action<CheatItem> ActionItemModify;
        public Action<string> ActionInput;
        public Action<int> ActionIntInput;
        public Action<string, string> ActionInput2;
        public List<CheatItem> Items = new List<CheatItem>();
        public string ActionInputValue;
        public string ActionInputValue2;
        public CheatItem(string title, Action action) { Title = title; Action = action; }
        public CheatItem(string title, Action<CheatItem> actionItemModify) { Title = title; ActionItemModify = actionItemModify; }
        public CheatItem(string title, Action<string> actionInput) { Title = title; ActionInput = actionInput; }
        public CheatItem(string title, Action<int> actionIntInput) { Title = title; ActionIntInput = actionIntInput; }
        public CheatItem(string title, Action<string, string> actionInput2) { Title = title; ActionInput2 = actionInput2; }
        public CheatItem(string title, List<CheatItem> items) { Title = title; Items = items; }
    }

    public class CheatLogic
    {
        #region Singletone

        private static volatile CheatLogic _instance = new CheatLogic();

        public static CheatLogic Instance
        {
            get { return _instance; }
        }

        static CheatLogic()
        { }

        #endregion

        public CheatItem RootItem;

        private MGame _game;
        private MData _data;

        public CheatLogic()
        {
            RootItem = new CheatItem("НАЗАД", new List<CheatItem>());
        }

        public async void Init()
        {
            _game = await Managers.ResolveAsync<MGame>();
            _data = await Managers.ResolveAsync<MData>();

            RootItem = new CheatItem("НАЗАД", new List<CheatItem>() {
#if !RELEASE_BUILD || USER_CHEAT
                new CheatItem("БОЙ", new List<CheatItem>() {
                    new CheatItem("Пропустить подготовку", SkipPrepare),
                    new CheatItem("Результат", new List<CheatItem>() {
                        new CheatItem("Победа", WinBattle),
                        new CheatItem("Поражение", LoseBattle),
                        new CheatItem("Ничья", DrawBattle),
                    }),
                    new CheatItem("Сменить оружие", new List<CheatItem>() {
                        new CheatItem("Слот 1", () => ChangeWeapon(0)),
                        new CheatItem("Слот 2", () => ChangeWeapon(1)),
                    }),
                }),

                new CheatItem("ПРОГРЕСС", new List<CheatItem>() {
                    new CheatItem("Пред. уровень", OpenPrevLevel),
                    new CheatItem("След. уровень", OpenNextLevel),
                }),

                new CheatItem("РЕCУРСЫ", new List<CheatItem>() {
                    new CheatItem("Добавить 500 Монет", () => AddSomeSoftCoin("500")),
                    new CheatItem("Добавить N Монет", (value) => AddSomeSoftCoin(value)),
                }),

                new CheatItem("СКИНЫ", new List<CheatItem>() {
                    new CheatItem("Открыть все", OpenAllSkins)
                }),
                new CheatItem("Удалить аккаунт", ClearAccount),
#endif
            });
        }

        public void ChangeWeapon(int index)
        {

        }

        public void SkipPrepare()
        {
        }

        public void WinBattle()
        {
        }

        public void LoseBattle()
        {
        }

        public void DrawBattle()
        {
        }

        public void OpenPrevLevel()
        {
            _data.Level.Value--;
            _game.OpenLobby();
        }

        public void OpenNextLevel()
        {
            _data.Level.Value++;
            _game.OpenLobby();
        }

        public void AddSomeSoftCoin(string value)
        {
            _data.Money.Value += int.Parse(value);
        }

        public void AddWeaponExp(string value)
        {
        }

        public void OpenAllSkins()
        {
        }

        public void ClearAccount()
        {
            _data.ResetData();
#if UNITY_EDITOR
            EditorApplication.ExecuteMenuItem("Edit/Play");
#else
            UnityEngine.Application.Quit();
#endif
        }

        public void SkipTutorial()
        {
        }
#endif
    }
}

