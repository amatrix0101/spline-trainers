﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;
    using Utils;
    
    public class CheatMenu : MonoBehaviour
    {

#if !RELEASE_BUILD || USER_CHEAT
        [SerializeField]
        private GameObject _itemPrefab;

        [SerializeField]
        private GameObject _itemsContainer;

        [SerializeField]
        private Image _lockMenuImage;

        [SerializeField]
        private GameObject _inputBlock;

        [SerializeField]
        private GameObject _botBlock;

        [SerializeField]
        private Text _inputText;

        [SerializeField]
        private List<Toggle> botBlockToggles;

        private List<CheatItemView> _views = new List<CheatItemView>();

        private CheatItem _current;
        private List<CheatItem> _path = new List<CheatItem>();

        public bool LockMode = false;

        private CheatItem _cheatItemForInput;

        private void Awake()
        {
            _current = CheatLogic.Instance.RootItem;
            CreateMenu();
        }

        private void CreateMenu()
        {
            foreach (CheatItemView view in _views)
            {
                Destroy(view.gameObject);
            }
            _views.Clear();

            if (_path.Count > 0)
            {
                CheatItemView view = _itemPrefab.InstantiateInParent<CheatItemView>(_itemsContainer.transform);
                view.InitAsBack(this, _path.Last(), _current);
                _views.Add(view);
            }

            foreach (CheatItem item in _current.Items)
            {
                CheatItemView view = _itemPrefab.InstantiateInParent<CheatItemView>(_itemsContainer.transform);
                view.Init(this, item);
                _views.Add(view);
            }
        }

        public void Close()
        {
            Destroy(gameObject);
        }

        public void OnBack()
        {
            _current = _path.Last();
            _path.RemoveAt(_path.Count - 1);
            CreateMenu();
        }

        public void OnForward(CheatItem item)
        {
            _path.Add(_current);
            _current = item;
            CreateMenu();
        }

        public void OnLockMenu()
        {
            LockMode = !LockMode;
            _lockMenuImage.color += new Color(0, 0, 0, 0.25f * (LockMode ? 1 : -1));
        }

        public void EnableInputMode(CheatItem cheatItemForInput)
        {
            _cheatItemForInput = cheatItemForInput;
            _inputBlock.SetActive(true);
        }

        public void OnInputSubmit()
        {
            if (_cheatItemForInput == null) return;
            try
            {
                Floor18Log.Log("Execute cheat: " + _cheatItemForInput.Title + " with " + _inputText.text);

                _cheatItemForInput.ActionInput(_inputText.text);

                _cheatItemForInput = null;
                _inputBlock.SetActive(false);

                if (!LockMode)
                    Close();
            }
            catch(System.Exception e)
            {
                Floor18Log.LogException(e);
                Close();

            }
        }

#endif
    }
}
