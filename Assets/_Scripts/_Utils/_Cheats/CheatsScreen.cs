﻿namespace Floor18.HyperCasualTemplate.Gui
{
    using UnityEngine;
    using UnityEngine.UI;
    using Floor18.Gui;
    using Floor18.Gui.Canvases;
    
    public class CheatsScreen : GuiController
    {
#if !RELEASE_BUILD || USER_CHEAT
        [SerializeField] private Button _button;
        [SerializeField] private GameObject _cheatMenuPrefab;

        public static bool Error { get; set; }
        private static bool _error;

        private static readonly Color _lightGreen = new Color(0.196f, 0.804f, 0.196f);
        private static readonly Color _lightRed = new Color(0.804f, 0.196f, 0.196f);
        private static readonly ColorBlock _greenBlock = new ColorBlock
        {
            normalColor = _lightGreen,
            highlightedColor = _lightGreen,
            pressedColor = Color.green,
            disabledColor = Color.gray,
            colorMultiplier = 1f,
            fadeDuration = 0.1f
        };

        private static readonly ColorBlock _redBlock = new ColorBlock
        {
            normalColor = _lightRed,
            highlightedColor = _lightRed,
            pressedColor = Color.red,
            disabledColor = Color.gray,
            colorMultiplier = 1f,
            fadeDuration = 0.1f
        };
#endif
        protected override void Awake()
        {
            base.Awake();

#if !RELEASE_BUILD || USER_CHEAT
            _error = Error;
            UpdateColorBlock();
#endif
        }

        void Update()
        {
#if !RELEASE_BUILD || USER_CHEAT
            if (Error == _error)
                return;
            _error = Error;
            UpdateColorBlock();
#endif
        }

#if !RELEASE_BUILD || USER_CHEAT
        public void Button1ClickHandler()
        {
            Error = _error = false;
            UpdateColorBlock();
        }

        private void UpdateColorBlock()
        {
            if (_button == null)
                return;
            _button.colors = Error ? _redBlock : _greenBlock;

            float alpha = Error ? 1f : 0.25f;
            _button.image.color = new Color(_button.image.color.r, _button.image.color.g, _button.image.color.b, alpha);
        }

        CheatMenu _currentCheatMenuView;
        public void OnCheatMenu()
        {
            if (_currentCheatMenuView != null)
            {
                Destroy(_currentCheatMenuView.gameObject);
                _currentCheatMenuView = null;
            }
            else
            {
                _currentCheatMenuView = _cheatMenuPrefab.InstantiateInParent<CheatMenu>(StaticCanvas.Instance.transform);
                (_currentCheatMenuView.transform as RectTransform).offsetMin = Vector2.zero;
                (_currentCheatMenuView.transform as RectTransform).offsetMax = Vector2.zero;
            }
        }
#endif
    }
}


