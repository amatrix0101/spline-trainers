// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BP-Mobile/SeaSimple"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_spped("spped", Range( -1 , 1)) = 0.1
		_Vector0("Vector 0", Vector) = (5,5,0,0)
		_Color0("Color 0", Color) = (0.6415094,0.6415094,0.6415094,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform sampler2D _TextureSample0;
		uniform float2 _Vector0;
		uniform float _spped;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 temp_cast_0 = (_spped).xx;
			float2 panner1 = ( _Time.y * temp_cast_0 + float2( 0,0 ));
			float2 uv_TexCoord6 = i.uv_texcoord * _Vector0 + panner1;
			o.Albedo = ( _Color0 * tex2D( _TextureSample0, uv_TexCoord6 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15600
1920;0.5;1908;1013;1600.15;513.324;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;5;-1860.595,22.3412;Float;False;Property;_spped;spped;1;0;Create;True;0;0;False;0;0.1;0.03;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;4;-1843.696,324.5412;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;1;-1637.095,112.1413;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;7;-1523.26,-75.4101;Float;False;Property;_Vector0;Vector 0;2;0;Create;True;0;0;False;0;5,5;9,9;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-1430.26,89.5899;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-1053.295,12.04118;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;b1f06e73a309a7241a981fee1cb8b78a;b2adb75a7631fa942bde41443dd56dcb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-758.5529,-168.9276;Float;False;Property;_Color0;Color 0;3;0;Create;True;0;0;False;0;0.6415094,0.6415094,0.6415094,0;0,0.6375059,0.7704455,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-533.5529,92.07242;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;BP-Mobile/SeaSimple;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;1;2;5;0
WireConnection;1;1;4;2
WireConnection;6;0;7;0
WireConnection;6;1;1;0
WireConnection;2;1;6;0
WireConnection;11;0;8;0
WireConnection;11;1;2;0
WireConnection;0;0;11;0
ASEEND*/
//CHKSM=F577F15B15C87DE6904E2439463BFE14B3BB6954